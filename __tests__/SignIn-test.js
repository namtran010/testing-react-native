import 'react-native'
import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { render, fireEvent, cleanup } from '@testing-library/react-native'
import { Provider } from 'react-redux'
import createStore from '../App/Redux'
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer'

// Components
import SignInScreen from '../App/Containers/SignInScreen/SignInScreen'
import PTextInput from '../App/Components/TextInput/PTextInput'
import PImage from '../App/Components/Image/PImage'
import {
  PCustomButton,
  PIconTextButton,
  PTextButton
} from '../App/Components/Button'
import PLoadingView from '../App/Components/Loading/PLoadingView'

// Redux
//import { expectSaga, testSaga } from 'redux-saga-test-plan'
//import { signIn } from '../App/Sagas/AuthSagas'
//import { put } from 'redux-saga/effects'
//import AuthActions from '../App/Redux/AuthRedux'

//Functions
import {
  validateEmail,
  isStandardPassword
} from '../App/Functions/UserFunctions'

// Styles + Themes
import stylesPPassword from '../App/Components/TextInput/Styles/PPasswordInputStyles'
import signUpStyles from '../App/Containers/SignUpScreen/Styles/SignUpScreenStyles'
import signInStyles from '../App/Containers/SignInScreen/Styles/SignInScreenStyles'
import Images from '../App/Themes/Images'

// Language
import { translate } from '../App/Language'

// Service
import AuthApi from '../App/Services/AuthApi'
import PPasswordInput from '../App/Components/TextInput/PPasswordInput';

configure({ adapter: new Adapter() })

// create our store
const store = createStore()

const arrowBack = require('../App/Images/Icons/arrowBack.png')
const showPassword = require('../App/Images/Icons/showPassword@2x.png')
const facebookLogo = require('../App/Images/Icons/facebook@2x.png')
const appleLogo = require('../App/Images/Icons/appleLogo.png')

jest.mock('react-native-orientation-locker', () => {})
jest.mock('@react-native-community/netinfo', () => {})
jest.mock('@invertase/react-native-apple-authentication', () => {})
jest.mock('@react-native-community/async-storage', () => {})
jest.mock('react-native-mov-to-mp4', () => {})
jest.mock('rn-fetch-blob', () => {})
jest.mock('react-native-fs', () => {})
jest.mock('react-native-share', () => {
  return {
    open: jest.fn()
  }
})
jest.mock('react-native-localize', () => {
  return {
    getLocales: jest.fn()
  }
})
jest.mock('react-native-device-info', () => {
  return { getModel: jest.fn() }
})
jest.mock('@react-native-firebase/admob', () => {
  return {
    InterstitialAd: jest.fn(),
    TestIds: jest.fn(),
    AdEventType: jest.fn(),
    RewardedAd: {
      createForAdRequest: jest.fn()
    }
  }
})
jest.mock('react-native-gesture-handler', () => {
  const View = require('react-native/Libraries/Components/View/View')
  return {
    Swipeable: View,
    DrawerLayout: View,
    State: {},
    ScrollView: View,
    Slider: View,
    Switch: View,
    TextInput: View,
    ToolbarAndroid: View,
    ViewPagerAndroid: View,
    DrawerLayoutAndroid: View,
    WebView: View,
    NativeViewGestureHandler: View,
    TapGestureHandler: View,
    FlingGestureHandler: View,
    ForceTouchGestureHandler: View,
    LongPressGestureHandler: View,
    PanGestureHandler: View,
    PinchGestureHandler: View,
    RotationGestureHandler: View,
    /* Buttons */
    RawButton: View,
    BaseButton: View,
    RectButton: View,
    BorderlessButton: View,
    /* Other */
    FlatList: View,
    gestureHandlerRootHOC: jest.fn(),
    Directions: {}
  }
})
jest.mock('@react-navigation/native', () => {
  return {
    ...jest.requireActual('@react-navigation/native'),
    useNavigation: () => ({
      navigate: jest.fn()
    })
  }
})

afterEach(cleanup)

describe('<SignInScreen /> component', () => {
  it('test render SignIn Screen', () => {
    const { toJSON } = render(
      <Provider store={store}>
        <SignInScreen />
      </Provider>
    )
    expect(toJSON()).toMatchSnapshot()
  })
  it('should render goBack button', () => {
    const onGoBack = jest.fn()
    const snap = renderer
      .create(
        <TouchableOpacity onPress={onGoBack} style={signUpStyles.backBtn}>
          <PImage
            source={Images.arrowBack}
            imageStyles={signUpStyles.backIcon}
          />
        </TouchableOpacity>
      )
      .toJSON()
    expect(snap).toMatchSnapshot()
  })
  it('should render email field textInput', () => {
    const handleChangeEmail = jest.fn()
    const onSubmitTypeEmail = jest.fn()
    const snap = renderer.create(
      <PTextInput
        onChangeText={handleChangeEmail}
        value={'hoang-nam.tran@ant-tech.eu'}
        keyboardType={'email-address'}
        placeholder={'email'}
        inputStyles={signUpStyles.distanceInput}
        returnKeyType={'next'}
        onSubmitEditing={onSubmitTypeEmail}
      />
    )
    expect(snap).toMatchSnapshot()
  })
  it('should render password field textInput', () => {
    const handleChangePassword = jest.fn()
    const onSubmitEditing = jest.fn()
    const snap = renderer
      .create(
        <PPasswordInput
          onChangeText={handleChangePassword}
          value={''}
          onSubmitEditing={onSubmitEditing}
        />
      )
      .toJSON()
    expect(snap).toMatchSnapshot()
  })
  it('should render forgotPassword button', () => {
    const onPressToForgotPassword = jest.fn()
    const snap = renderer.create(
      <TouchableOpacity onPress={onPressToForgotPassword}>
        <Text style={signInStyles.forgotPassText}>Forgot your password ?</Text>
      </TouchableOpacity>
    )
    expect(snap).toMatchSnapshot()
  })
  it('should render SignIn button', () => {
    const handleLogin = jest.fn()
    const snap = renderer.create(
      <PTextButton
        buttonStyle={[signInStyles.btn, signInStyles.signUpExtra]}
        text={'Sign In'}
        textStyle={signInStyles.btnText}
        onPress={handleLogin}
      />
    )
    expect(snap).toMatchSnapshot()
  })
  it('should render SignInWithFB button', () => {
    const onPressLoginWithFB = jest.fn()
    const snap = renderer.create(
      <PIconTextButton
        buttonStyle={[signInStyles.signInBtn, signInStyles.signInFacebook]}
        onPress={onPressLoginWithFB}
        iconSource={facebookLogo}
        iconStyle={signInStyles.btnImage}
        title={'facebookLogin'}
        textStyle={signInStyles.btnText}
      />
    )
    expect(snap).toMatchSnapshot()
  })
  it('should render SignInWithApple button', () => {
    const onAppleButtonPress = jest.fn()
    const snap = renderer.create(
      <PIconTextButton
        buttonStyle={[signInStyles.signInBtn, signInStyles.signInApple]}
        onPress={onAppleButtonPress}
        iconSource={appleLogo}
        iconStyle={signInStyles.btnImage}
        title={'signInApple'}
        textStyle={signInStyles.btnText}
      />
    )
    expect(snap).toMatchSnapshot()
  })
  it('should render Activity Indicator ', () => {
    const snap = renderer.create(<PLoadingView />).toJSON()
    expect(snap).toMatchSnapshot()
  })
  it('should call onPress SignInApple button', () => {
    const onAppleButtonPress = jest.fn()
    const wrapper = shallow(
      <PIconTextButton
        buttonStyle={[signInStyles.signInBtn, signInStyles.signInApple]}
        onPress={onAppleButtonPress}
        iconSource={appleLogo}
        iconStyle={signInStyles.btnImage}
        title={'signInApple'}
        textStyle={signInStyles.btnText}
      />
    )
    wrapper.simulate('press')
    expect(onAppleButtonPress).toHaveBeenCalled()
  })
  it('should call onPress SignInFB button', () => {
    const onPressLoginWithFB = jest.fn()
    const wrapper = shallow(
      <PIconTextButton
        buttonStyle={[signInStyles.signInBtn, signInStyles.signInFacebook]}
        onPress={onPressLoginWithFB}
        iconSource={facebookLogo}
        iconStyle={signInStyles.btnImage}
        title={'facebookLogin'}
        textStyle={signInStyles.btnText}
      />
    )
    wrapper.simulate('press')
    expect(onPressLoginWithFB).toHaveBeenCalled()
  })
  it('should call onPress SignIn button', () => {
    const handleLogin = jest.fn()
    const wrapper = shallow(
      <PTextButton
        buttonStyle={[signInStyles.btn, signInStyles.signUpExtra]}
        text={'Sign In'}
        textStyle={signInStyles.btnText}
        onPress={handleLogin}
      />
    )
    wrapper.simulate('press')
    expect(handleLogin).toHaveBeenCalled()
  })
  it('should call onPress forgotPassword button', () => {
    const onPressToForgotPassword = jest.fn()
    const wrapper = shallow(
      <TouchableOpacity onPress={onPressToForgotPassword}>
        <Text style={signInStyles.forgotPassText}>Forgot your password ?</Text>
      </TouchableOpacity>
    )
    wrapper.simulate('press')
    expect(onPressToForgotPassword).toHaveBeenCalled()
  })
  it('should call onPress hideOrShowPassword button', () => {
    const onHideOrShowPassword = jest.fn()
    const wrapper = shallow(
      <PCustomButton
        source={showPassword}
        imageStyle={stylesPPassword.passwordIcon}
        onPress={onHideOrShowPassword}
      />
    )
    wrapper.simulate('press')
    expect(onHideOrShowPassword).toHaveBeenCalled()
  })
  it('should call onPress goBack button', () => {
    const onGoBack = jest.fn()
    const wrapper = shallow(
      <TouchableOpacity onPress={onGoBack} style={signUpStyles.backBtn}>
        <PImage source={arrowBack} imageStyles={signUpStyles.backIcon} />
      </TouchableOpacity>
    )
    wrapper.simulate('press')
    expect(onGoBack).toHaveBeenCalled()
  })
  it('textInput Email onChangeText', () => {
    const EMAIL = 'namlaem98@gmail.com'
    const { getByPlaceholderText } = render(
      <Provider store={store}>
        <SignInScreen />
      </Provider>
    )
    // change text
    const inputText = getByPlaceholderText(translate('email'))
    fireEvent.changeText(inputText, EMAIL)
    // expect text value to equal EMAIL
    const valueEmail = inputText.props.defaultValue
    expect(valueEmail).toBe(EMAIL)
  })
  it('textInput Password onChangeText', () => {
    const PASSWORD = '123456789'
    const { getByPlaceholderText } = render(
      <Provider store={store}>
        <SignInScreen />
      </Provider>
    )
    // change text
    const inputText = getByPlaceholderText(translate('enterPassword'))

    fireEvent.changeText(inputText, PASSWORD)
    // expect text value to equal PASSWORD
    const valuePassword = inputText.props.value
    expect(valuePassword).toBe(PASSWORD)
  })
  //it('go back button onPress', () => {
  //  const { getByTestId } = render(
  //    <Provider store={store}>
  //      <SignInScreen />
  //    </Provider>
  //  )
  //  const button = getByTestId('go back')
  //  fireEvent.press(button)
  //})
  //it('Login function', async () => {
  //  const { getByTestId, getByPlaceholderText, queryByTestId } = render(
  //    <Provider store={store}>
  //      <SignInScreen />
  //    </Provider>
  //  )
  //  const inputEmail = getByPlaceholderText(translate('email'))
  //  expect(inputEmail).toBeTruthy()
  //  const textEmailToEnter = 'namlaem98@gmail.com'
  //  fireEvent.changeText(inputEmail, textEmailToEnter)
  //  const inputPassword = getByPlaceholderText(translate('enterPassword'))
  //  expect(inputPassword).toBeTruthy()
  //  const textPasswordToEnter = '123456789'
  //  fireEvent.changeText(inputPassword, textPasswordToEnter)
  //  const button = getByTestId('button login')
  //  fireEvent.press(button)
  //  //expect(await store.getState().auth.userData).toBeNull()
  //})
})

describe('SignIn function', () => {
  it('validate email function', () => {
    expect(validateEmail('namlaem98@gmail.com')).toBeTruthy()
  })
  it('is invalid email', () => {
    expect(validateEmail('namlaem98@')).toBeFalsy()
  })
  it('is standard password', () => {
    expect(isStandardPassword('123123123')).toBeTruthy()
  })
  it('is not standard password', () => {
    expect(isStandardPassword('1223123')).toBeFalsy()
  })
  it('api login', async () => {
    const api = AuthApi.create()
    const userInfo = { email: 'namlaem98@gmail.com', password: '123456789' }
    const response = await api.signIn(userInfo)
    expect(response.status).toEqual(201)
    expect(response.ok).toBeTruthy()
    expect(response.data).not.toBeNull()
    //return expectSaga(signIn, api, userInfo)
    //  .put({
    //    type: 'SIGN_IN_FAILURE',
    //    errorSignIn: "Cannot read property 'email' of undefined"
    //  })
    //  .dispatch({ type: 'SIGN_IN_FAILURE', payload: 'HELLO' })
    //  .run()
  })
})
